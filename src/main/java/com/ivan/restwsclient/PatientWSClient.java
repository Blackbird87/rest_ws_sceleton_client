package com.ivan.restwsclient;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ivan.restwsclient.model.Patient;

public class PatientWSClient {

	
	private static final String PATIENT_REST_WS_URL = "http://localhost:8080/restws/services/patientservice";

	
	
	/**
	 * Starting point for the program
	 * @param args
	 */
	public static void main(String[] args) {

		Client client = ClientBuilder.newClient();
		PatientWSClient patientWs = new PatientWSClient();
		Patient patient = patientWs.getPatientById(1, client);
		System.out.println(patient.toString());

		Patient patient2 = new Patient();
		patient2.setName("Grigor");

		patientWs.createPatient(patient2, client);
		
		patientWs.deletePatientById(1 , client);

		client.close();

	}
	
	/**
	 * Update patient based on the Patient id
	 * @param patient
	 * @param client
	 * @return 200 if OK, 404 if not found
	 */
	public boolean updatePatient(Patient patient, Client client) {
		Response updateResponse = null;

		try {
			WebTarget putTarget = client.target(PATIENT_REST_WS_URL).path("/patients/");
			updateResponse = putTarget.request().put(Entity.entity(patient, MediaType.APPLICATION_XML));
			System.out.println(updateResponse.getStatus());
			if (updateResponse.getStatus() == 200) {
				return true;
			}
		} catch (Exception e) {

		} finally {
			if (updateResponse != null)
				updateResponse.close();
		}
		return false;

	}
	
	/**
	 * Will create a new Patient
	 * @param patient
	 * @param client
	 * @return new Patient if created successfully, or null, if unsuccessfully
	 */
	public boolean createPatient(Patient patient, Client client) {
		try {
			WebTarget createTarget = client.target(PATIENT_REST_WS_URL).path("/patients");
			Patient createdPatient = createTarget.request().post(Entity.entity(patient, MediaType.APPLICATION_XML),
					Patient.class);
			if (createdPatient != null) {
				System.out.println("New Patient was created");
				System.out.println(createdPatient.toString());
				return true;
			}
		} catch (Exception e) {

		}
		return false;
	}

	
	/**
	 * Will retrieve Patient object based on patientId
	 * @param patientId
	 * @param client
	 * @return Patient object
	 */
	public Patient getPatientById(int patientId, Client client) {
		Patient patient = null;
		try {
			WebTarget target = client.target(PATIENT_REST_WS_URL).path("/patients/").path("{id}").resolveTemplate("id",
					patientId);
			Builder request = target.request();
			patient = request.get(Patient.class);

		} catch (Exception e) {

		}
		return patient;
	}
	
	
	/**
	 * Will delete Patient object based on patientId
	 * @param patientId
	 * @param client
	 * @return true or false
	 */
	public boolean deletePatientById(int patientId, Client client) {
	
		Response deleteResponse = null ; 
		try {
			WebTarget deleteTarget = client.target(PATIENT_REST_WS_URL).path("/patients/").path("{id}").resolveTemplate("id",
					patientId);

		Invocation.Builder invocationBuilder = deleteTarget.request();
		deleteResponse = invocationBuilder.delete();
		if(deleteResponse.getStatus() == 200)
		{
			System.out.println("User with Id " + patientId + " delete successfully");
			return true ;
		}
		} catch (Exception e) {

		}
		finally {
			deleteResponse.close();
		}
		return false;
	}

}
